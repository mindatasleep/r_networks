#!/bin/bash
set -ex

# Install R
sudo yum install -y R R-core R-core-devel R-devel
sudo yum install -y curl
sudo yum install -y openssl
sudo yum -y install libcurl-devel
sudo yum -y install openssl-devel
sudo yum -y install libssh2-devel

# Install libraries
sudo Rscript /home/hadoop/code/mobility-studies/src/polyglot/r_networks/src/_install_packages.R



# DeviceMov_covid19mx

Device mobility (network) analyses

## Script descriptions

### NetworkReconstructor.R

Takes a data frame with device positions and time stamps

device_id | position - H3 - | timestamp

Returns two files:

- an rds file with bipartite networks: device - place
- a weighted network in a graphml file
  - with devices linked if they were # at the same place at the same time
  - (see https://arxiv.org/pdf/2007.14596.pdf)

### analysis_script_globalParameters.R

Takes a network in graphml
Returns a txt file (tab separated) with global properties of the network:

- num nodes
- num edges
- clustering coefficient
- diameter
- avg shortest paths

### analysis_script_NodeAndEdge.R

Takes a network in graphml
Returns a copy of the network in graphml format with

- degree centrality
- betweenness cent
- infomap communities
- edge betweenness (cutoff = 15, hardcoded)

## HotspotDetector.R

* Takes an rds file with bipartite networks (output 1 of NetworkConstructor)
* Returns a tsv file (.txt) 
  *  With the number of people who had at least one contact  in each H3 level 10 hexagon.
  *  Ranging from 2 upwards (0 or 1 person cannot form a contact alone)

## Activate conda environment (On Xiuhcoatl)

```
conda activate r4
```

## Write to S3

First, set credentials for IAM user `polyglot` as ENV variables. Find them in `src/.Renviron`.

These grant read and (basic) write access to the `polyglot-output` bucket.

```
install.packages("aws.s3", repos = "https://cloud.R-project.org")

library("aws.s3")

s3save(results, bucket="polyglot-output", object="del.csv")
```

## List/copy S3 files from CLI

First, set credentials for IAM user `polyglot` for an AWS CLI profile with the same name.

```
// List files
aws s3 ls s3://polyglot-output --profile polyglot

// Copy locally
aws s3 cp s3://polyglot-output/del.csv ./Downloads/ --profile polyglot
```
